package ru.hnau.suncamp.common.data


enum class TransactionCategory(
        val title: String
) {
    payment("платеж"),
    gift("подарок"),
    tax("налог"),
    fine("штраф"),
    encouraging("поощрение"),
    rollback("возврат")
}