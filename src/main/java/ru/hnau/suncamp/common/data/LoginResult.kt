package ru.hnau.suncamp.common.data


data class LoginResult(
        val authToken: String,
        val user: User
)