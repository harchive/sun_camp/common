package ru.hnau.suncamp.common.data


data class Card(
        val uuid: String,
        val associatedUserLogin: String
)