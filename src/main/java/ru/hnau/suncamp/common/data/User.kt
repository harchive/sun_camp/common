package ru.hnau.suncamp.common.data

data class User(
        val login: String,
        val type: UserType,
        val name: String
)