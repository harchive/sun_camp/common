package ru.hnau.suncamp.common.data


data class StorageManager(
        val masterUserLogin: String,
        val slaveUserLogin: String
)