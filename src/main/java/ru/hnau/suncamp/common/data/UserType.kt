package ru.hnau.suncamp.common.data

enum class UserType(
        val title: String,
        val allowManageUsersAccounts: Boolean,
        val allowManageStoragesAccounts: Boolean
) {

    admin(
            title = "администратор",
            allowManageUsersAccounts = true,
            allowManageStoragesAccounts = true
    ),

    manager(
            title = "управляющий",
            allowManageUsersAccounts = true,
            allowManageStoragesAccounts = true
    ),

    user(
            title = "пользователь",
            allowManageUsersAccounts = false,
            allowManageStoragesAccounts = false
    ),

    storage(
            title = "хранилище",
            allowManageUsersAccounts = false,
            allowManageStoragesAccounts = false
    )
}