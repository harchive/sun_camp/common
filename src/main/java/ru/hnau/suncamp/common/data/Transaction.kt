package ru.hnau.suncamp.common.data


data class Transaction(
        val id: Long,
        val fromUserLogin: String,
        val toUserLogin: String,
        val initiatorUserLogin: String,
        val amount: Long,
        val category: TransactionCategory,
        val timestamp: Long,
        val comment: String,
        val associatedTransactionId: Long?
)