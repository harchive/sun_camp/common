package ru.hnau.suncamp.common.validators

import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType


object UserNameValidator {

    const val MIN_USER_NAME_LENGTH = 5
    const val MAX_USER_NAME_LENGTH = 60
    val AVAILABLE_CHARS = ValidatorsUtils.LETTERS + ValidatorsUtils.SPACE

    @Throws(ApiErrorException::class)
    fun validateUserName(userName: String) {

        val length = userName.length
        if (length < MIN_USER_NAME_LENGTH) {
            throw ApiErrorException(ApiErrorType.INCORRECT_USER_NAME, "Минимальная длина ФИО - $MIN_USER_NAME_LENGTH")
        }

        if (length > MAX_USER_NAME_LENGTH) {
            throw ApiErrorException(ApiErrorType.INCORRECT_USER_NAME, "Максимальная длина ФИО - $MAX_USER_NAME_LENGTH")
        }

        val incorrectChar = userName.find { it !in AVAILABLE_CHARS }
        if (incorrectChar != null) {
            throw ApiErrorException(ApiErrorType.INCORRECT_USER_NAME, "Некорректный символ - '$incorrectChar'")
        }

    }

}