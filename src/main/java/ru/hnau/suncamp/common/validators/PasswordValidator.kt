package ru.hnau.suncamp.common.validators

import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType


object PasswordValidator {

    const val MIN_PASSWORD_LENGTH = 4
    const val MAX_PASSWORD_LENGTH = 30
    val AVAILABLE_CHARS = ValidatorsUtils.LETTERS + ValidatorsUtils.DIGITS + ValidatorsUtils.ADDITIONAL_SYMBOLS

    @Throws(ApiErrorException::class)
    fun validatePassword(password: String) {

        val length = password.length
        if (length < MIN_PASSWORD_LENGTH) {
            throw ApiErrorException(ApiErrorType.INCORRECT_PASSWORD, "Минимальная длина пароля - $MIN_PASSWORD_LENGTH")
        }

        if (length > MAX_PASSWORD_LENGTH) {
            throw ApiErrorException(ApiErrorType.INCORRECT_PASSWORD, "Максимальная длина пароля - $MAX_PASSWORD_LENGTH")
        }

        val incorrectChar = password.find { it !in AVAILABLE_CHARS }
        if (incorrectChar != null) {
            throw ApiErrorException(ApiErrorType.INCORRECT_PASSWORD, "Некорректный символ - '$incorrectChar'")
        }

    }

}