package ru.hnau.suncamp.common.validators


object ValidatorsUtils {

    const val ENGLISH_LETTERS_LOWERCASE = "abcdefghijklmnopqrstuvwxyz"
    const val ENGLISH_LETTERS_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    const val ENGLISH_LETTERS = ENGLISH_LETTERS_LOWERCASE + ENGLISH_LETTERS_UPPERCASE

    const val RUSSIAN_LETTERS_LOWERCASE = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
    const val RUSSIAN_LETTERS_UPPERCASE = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    const val RUSSIAN_LETTERS = RUSSIAN_LETTERS_LOWERCASE + RUSSIAN_LETTERS_UPPERCASE

    const val LETTERS_LOWERCASE = ENGLISH_LETTERS_LOWERCASE + RUSSIAN_LETTERS_LOWERCASE
    const val LETTERS_UPPERCASE = ENGLISH_LETTERS_UPPERCASE + RUSSIAN_LETTERS_UPPERCASE
    const val LETTERS = LETTERS_LOWERCASE + LETTERS_UPPERCASE

    const val DIGITS = "0123456789"
    const val ADDITIONAL_SYMBOLS = "!@#\$%^&*_+-=№%?/|()<>{}[]"
    const val SPACE = " "

}