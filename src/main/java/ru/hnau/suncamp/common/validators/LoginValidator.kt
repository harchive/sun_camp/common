package ru.hnau.suncamp.common.validators

import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType


object LoginValidator {

    const val MIN_LOGIN_LENGTH = 3
    const val MAX_LOGIN_LENGTH = 30
    val AVAILABLE_CHARS = ValidatorsUtils.LETTERS + ValidatorsUtils.DIGITS + ValidatorsUtils.ADDITIONAL_SYMBOLS

    @Throws(ApiErrorException::class)
    fun validateLogin(login: String) {

        val length = login.length
        if (length < MIN_LOGIN_LENGTH) {
            throw ApiErrorException(ApiErrorType.INCORRECT_LOGIN, "Минимальная длина логина - $MIN_LOGIN_LENGTH")
        }

        if (length > MAX_LOGIN_LENGTH) {
            throw ApiErrorException(ApiErrorType.INCORRECT_LOGIN, "Максимальная длина логина - $MAX_LOGIN_LENGTH")
        }

        val incorrectChar = login.find { it !in AVAILABLE_CHARS }
        if (incorrectChar != null) {
            throw ApiErrorException(ApiErrorType.INCORRECT_LOGIN, "Некорректный символ - '$incorrectChar'")
        }

    }

}