package ru.hnau.suncamp.common.api.error


class ApiErrorException(
        val apiError: ApiError,
        cause: Throwable? = null
) : Exception(
        apiError.type.name + ": " + apiError.text,
        cause
) {

    constructor(
            type: ApiErrorType,
            text: String = type.defaultText,
            params: Map<String, String>? = null,
            cause: Throwable? = null
    ) : this(
            ApiError(type, text, params),
            cause
    )

}