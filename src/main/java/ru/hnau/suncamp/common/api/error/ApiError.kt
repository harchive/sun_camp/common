package ru.hnau.suncamp.common.api.error


data class ApiError(
        val type: ApiErrorType,
        val text: String = type.defaultText,
        val params: Map<String, String>? = null
) {

    companion object {

        val DEFAULT = ApiError(ApiErrorType.UNDEFINED)

    }

}