package ru.hnau.suncamp.common.api

import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncamp.common.api.error.ApiError
import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType


class ApiResult<T : Any> private constructor(
        val data: T? = null,
        val error: ApiError? = null
) {

    companion object {

        fun <T : Any> success(data: T) =
                ApiResult(data = data)

        fun error(error: ApiError) =
                ApiResult<Any>(error = error)

        fun error(type: ApiErrorType, text: String = type.defaultText, params: Map<String, String>? = null) =
                error(ApiError(type, text, params))

        fun error(text: String = ApiErrorType.UNDEFINED.defaultText, params: Map<String, String>? = null) =
                error(ApiErrorType.UNDEFINED, text, params)

        fun <T : Any> trySuccess(data: T?, errorIfNoData: ApiError = ApiError.DEFAULT) =
                if (data == null) error(errorIfNoData) else success(data)

        fun <T : Any> trySuccess(data: T?, errorTypeIfNoData: ApiErrorType = ApiErrorType.UNDEFINED, errorTextIfNoData: String = errorTypeIfNoData.defaultText) =
                if (data == null) error(ApiError(errorTypeIfNoData, errorTextIfNoData)) else success(data)

    }


    fun <R : Any> handle(
            onSuccess: (T) -> R,
            onError: (ApiError) -> R
    ) =
            if (data != null) onSuccess.invoke(data) else onError.invoke(error ?: ApiError.DEFAULT)

    override fun toString() = "ApiResult{data=$data, error=$error}"

}


