package ru.hnau.suncamp.common.api

import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncamp.common.api.error.ApiErrorException

val <T : Any> ApiResult<T>.possible: Possible<T>
    get() = handle(
            onSuccess = { Possible.success(it) },
            onError = { Possible.error(ApiErrorException(it)) }
    )

fun <T : Any> Finisher<ApiResult<T>>.toPossibleFinisher() = map { apiResult -> apiResult.possible }