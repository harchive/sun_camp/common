package ru.hnau.suncamp.common.api.response

import ru.hnau.suncamp.common.data.Transaction


data class SubordinateStorageInfo(
        val storageUserLogin: String,
        val balance: Long,
        val transactions: List<Transaction>
)