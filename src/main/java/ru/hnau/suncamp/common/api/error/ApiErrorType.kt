package ru.hnau.suncamp.common.api.error


enum class ApiErrorType(
        val defaultText: String
) {

    UNDEFINED("Неизвестная ошибка"),
    INCORRECT_LOGIN_OR_PASSWORD("Неверный логин или пароль"),
    NEED_LOGIN("Необходимо авторизоваться"),
    INCORRECT_LOGIN("Некорректный логин"),
    INCORRECT_PASSWORD("Некорректный пароль"),
    INCORRECT_USER_NAME("Некорректные ФИО пользователя"),
    LOGIN_ALREADY_REGISTERED("Логин занят"),
    INCORRECT_CARD_UUID("Некорректный идентификатор карты"),
    CARD_ALREADY_REGISTERED("Карта уже зарегистрирована"),
    USER_NOT_FOUND("Пользователь не найден"),
    CARD_NOT_FOUND("Карта не найдена"),
    ACCOUNT_NOT_FOUND("Счет не найден"),
    TRANSACTION_NOT_FOUND("Транзакция не найдена"),
    ACCOUNT_OUTCOME_INSUFFICIENT_BALANCE("Недостаточно средств на счету"),
    AMOUNT_MUST_BE_POSITIVE("Сумма должна быть положительной"),
    CIRCLE_TRANSACTION_NOT_SUPPORTED("Нельзя переводить самому себе"),
    PERMISSION_DENIED("Недостаточно прав"),
    UNSUPPORTED_VERSION("Неподдерживаемая версия приложения"),
    UNABLE_TO_MANAGE_NOT_STORAGE_USER("Невозможно управлять не хранилищем"),
    UNSUPPORTED_USER_TYPE_FOR_MANAGING_STORAGE("Недопустимый тип пользователя для управления хранилищем"),
    IP_BLOCKED("Ip адрес заблокирован за чрезмерную активность"),

}